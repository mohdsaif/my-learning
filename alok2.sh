echo "Backup scripted has started"
cp /etc/nginx /home/root/backup
if [[ "$1" == "-v" ]]; then
	echo "created backup"
fi

curl https://aws.amazon.com/dss/dfds -f /home/root/backup 
if [[ "$1" == "-v" ]]; then
        echo "Backup pushed"
fi

date=`date`
email "manager@noodle.ai" "backup done for ${date}"
if [[ "$1" == "-v" ]]; then
        echo "Backup done, email send"
fi
echo "script completed successfully"
